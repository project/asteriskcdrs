<?php


/**********************************************************************
 * Name: asteriskcdrs_admin_extra.inc
 * Author: Code Generator
 * Date: 2008-01-21
 * 
 * Description: Extra Administrative functions for asteriskcdrs
 *              Drupal Module.
 *********************************************************************/


/**
 * Additional Admin functions for the Asterisk CDRs
 * Module should be added to this file, in case you wish to re-generate
 * these files again later.
 */

/**
 * Asterisk CDRs Administrative Settings Page
 */
function asteriskcdrs_admin_extra_settings() {
	$items = array();

	$items['asteriskcdrs_cdr_database'] = array(
		'#type'          => 'textfield',
		'#title'         => t('Asterisk CDR Database'),
		'#size'          => 25,
		'#maxlength'     => 255,
		'#default_value' => variable_get('asteriskcdrs_cdr_database', db_result(db_query("SELECT DATABASE()"))),
		'#description'   => t('Which database is your CDR table housed in?'),
	);

	$table_columns = array(
		'cdrid'         => 'cdrid'
		, 'calldate'    => 'calldate'
		, 'clid'        => 'clid'
		, 'src'         => 'src'
		, 'dst'         => 'dst'
		, 'dcontext'    => 'dcontext'
		, 'channel'     => 'channel'
		, 'dstchannel'  => 'dstchannel'
		, 'lastapp'     => 'lastapp'
		, 'lastdata'    => 'lastdata'
		, 'duration'    => 'duration'
		, 'billsec'     => 'billsec'
		, 'disposition' => 'disposition'
		, 'amaflags'    => 'amaflags'
		, 'accountcode' => 'accountcode'
		, 'uniqueid'    => 'uniqueid'
		, 'userfield'   => 'userfield'
	);

	$items['asteriskcdrs_cdr_columns'] = array(
		'#type'          => 'checkboxes',
		'#title'         => t('Asterisk CDR Columns'),
		'#options'       => $table_columns,
		'#default_value' => variable_get('asteriskcdrs_cdr_columns', array('calldate', 'clid', 'src', 'dst', 'duration')),
		'#description'   => t('Which columns would you like displayed from your CDR table when viewing groups of data in tables?  Note: Details call views will show all data.'),
	);

	return $items;
}


/**
 * asteriskcdrs_admin_fix_db()
 *
 *	Asterisk doesn't have any unique, indexable fields, by default.  For sanity
 * we want to add a new row, `cdrid`, as an auto_incrementing promary key.
 */
function asteriskcdrs_admin_fix_db() {
	//Page to force following command on database table:
	//ALTER TABLE cdr ADD COLUMN `cdrid` int(10) NOT NULL auto_increment, ADD CONSTRAINT PRIMARY KEY(`cdrid`);

	//Confirm that variable is unset, or set to zero
	//(we don't want to allow this query more than once)
	if(variable_get('asteriskcdrs_db_fix', 0) == 0){
		//Run query
		db_query('
			ALTER TABLE 
				' . variable_get('asteriskcdrs_cdr_database', db_result(db_query('SELECT DATABASE()'))) . '.cdr
			ADD COLUMN
				`cdrid` int(10) NOT NULL auto_increment
			FIRST
			, ADD CONSTRAINT PRIMARY KEY(`cdrid`)
		');
		//Set variable to 1, to not allow this to be run again.
		variable_set('asteriskcdrs_db_fix', 1);
	}
	//Redirect.
	drupal_goto('admin/asteriskcdrs');
}


/**
 * asteriskcdrs_admin_setup_db()
 * 
 * We will not install DB tables, by default, with this package.  Users likely
 * already have an Asterisk CDR table to keep call detail records, but if 
 * they're starting fresh, we'll use this function to allow them to set up
 * a new Database.
 */
function asteriskcdrs_admin_setup_db() {
	//Set up form.
	$items = array();
	$items['stop_bugging_me'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Database Setup Options'),
		'#options' => array(
			1 => t('I use a different database to store my Asterisk CDRs'),
			2 => t('I want to set up a new table in the Drupal Database to store my Asterisk CDRs')
		),
		'#description' => t('Check this box, and hit submit, if you store CDRs in a database outside of Drupal and we won\'t bug you again.  Please note: If you\'ve checked this box, be sure you have also configured this module to talk to the proper database'),
	);
	$items['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Continue'),
	);
	$items['cancel'] = array(
		'#type' => 'submit',
		'#value' => t('Cancel'),
	);

	return $items;
}


/**
 * asteriskcdrs_admin_setup_db_validate()
 *
 * If user didn't hit cancel, make sure that user had checked one, and only
 * one, box from the form.
 */
function asteriskcdrs_admin_setup_db_validate($form_id, $form_values) {
	if($form_values['op'] != t('Cancel')) {
		if($form_values['stop_bugging_me'][1] == 0 && $form_values['stop_bugging_me'][2] == 0)
		{
			form_set_error('stop_bugging_me', 'You must select one option!');
		} 
		elseif($form_values['stop_bugging_me'][1] != 0 && $form_values['stop_bugging_me'][2] != 0)
		{
			form_set_error('stop_bugging_me', 'You must select only one option!');
		} 
	}
}


/**
 * asteriskcdrs_date_range_submit()
 *
 * Perform requested task and return to main admin page.
 */
function asteriskcdrs_admin_setup_db_submit($form_id, $form_values) {
	$op = $form_values['op'];
	switch($op)
	{
		case t('Continue'):
			//Check if user is utilizing cdr table in another database.  If so,
			//Set variable and send them back to main admin page.
			if($form_values['stop_bugging_me'][1] != 0)
			{
				variable_set('asteriskcdrs_db_setup', 1);
				drupal_set_message('Thank you.  Your setting is saved, and we will not bug you again.');
				drupal_goto('admin/asteriskcdrs');
			}
			//Otherwise, determine database type, and install tables.
			switch ($GLOBALS['db_type']) {
				case 'mysql':
					db_query("DROP TABLE IF EXISTS {cdr}");
					db_query("CREATE TABLE {cdr} (
						`cdrid` int(10) NOT NULL auto_increment,
						`calldate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'  ,
						`clid` varchar(80) NOT NULL default '',
						`src` varchar(80) NOT NULL default '',
						`dst` varchar(80) NOT NULL default '',
						`dcontext` varchar(80) NOT NULL default '',
						`channel` varchar(80) NOT NULL default '',
						`dstchannel` varchar(80) NOT NULL default '',
						`lastapp` varchar(80) NOT NULL default '',
						`lastdata` varchar(80) NOT NULL default '',
						`duration` int(11) NOT NULL DEFAULT '0'  ,
						`billsec` int(11) NOT NULL DEFAULT '0'  ,
						`disposition` varchar(45) NOT NULL default '',
						`amaflags` int(11) NOT NULL DEFAULT '0'  ,
						`accountcode` varchar(20) NOT NULL default '',
						`uniqueid` varchar(32) NOT NULL default '',
						`userfield` varchar(255) NOT NULL default '',
						PRIMARY KEY (`cdrid`),
						KEY `calldate` (`calldate`),
						KEY `dst` (`dst`),
						KEY `accountcode` (`accountcode`)
					) ENGINE=MyISAM DEFAULT CHARSET=latin1;");
					drupal_set_message('Successfully installed database!  Make sure that your Asterisk setup points to ' . variable_get('asteriskcdrs_cdr_database', db_result(db_query('SELECT DATABASE()'))) . '.cdr');
					variable_set('asteriskcdrs_db_setup', 1);
					break;
				case 'mysqli':
					db_query("DROP TABLE IF EXISTS {cdr}");
					db_query("CREATE TABLE {cdr} (
						`cdrid` int(10) NOT NULL auto_increment,
						`calldate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'  ,
						`clid` varchar(80) NOT NULL default '',
						`src` varchar(80) NOT NULL default '',
						`dst` varchar(80) NOT NULL default '',
						`dcontext` varchar(80) NOT NULL default '',
						`channel` varchar(80) NOT NULL default '',
						`dstchannel` varchar(80) NOT NULL default '',
						`lastapp` varchar(80) NOT NULL default '',
						`lastdata` varchar(80) NOT NULL default '',
						`duration` int(11) NOT NULL DEFAULT '0'  ,
						`billsec` int(11) NOT NULL DEFAULT '0'  ,
						`disposition` varchar(45) NOT NULL default '',
						`amaflags` int(11) NOT NULL DEFAULT '0'  ,
						`accountcode` varchar(20) NOT NULL default '',
						`uniqueid` varchar(32) NOT NULL default '',
						`userfield` varchar(255) NOT NULL default '',
						PRIMARY KEY (`cdrid`),
						KEY `calldate` (`calldate`),
						KEY `dst` (`dst`),
						KEY `accountcode` (`accountcode`)
					) ENGINE=MyISAM DEFAULT CHARSET=latin1;");
					drupal_set_message('Successfully installed database!  Make sure that your Asterisk setup points to ' . variable_get('asteriskcdrs_cdr_database', db_result(db_query('SELECT DATABASE()'))) . '.cdr');
					variable_set('asteriskcdrs_db_setup', 1);
					break;
				case 'pgsql':
					drupal_set_message('Unable to install database.  We only have support for MySQL at this time!');
					variable_set('asteriskcdrs_db_setup', 0);
					break;
			}
			drupal_goto('admin/asteriskcdrs');
			break;
		case t('Cancel'):
			//User clicked cancel.  Send them back to main admin page, 
			//doing nothing.
			drupal_goto('admin/asteriskcdrs');
			break;
	}
}