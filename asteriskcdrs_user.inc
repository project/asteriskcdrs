<?php


/**********************************************************************
 * Name: asteriskcdrs_user.inc
 * Author: Code Generator
 * Date: 2008-01-21
 * 
 * Description: User functions for asteriskcdrs
 *              Drupal Module.
 *********************************************************************/


/**
 * asteriskcdrs_date_range()
 *
 * Form to choose criteria of Call Detail Records.
 */
function asteriskcdrs_date_range() {
	$form = array();

	$call_duration_options = array(
		'ac' => 'Search All Calls',
		'gt' => 'Longer Than',
		'lt' => 'Shorter Than',
		'eq' => 'Exactly',
	);

	$form['cdr_lookup'] = array(
		'#type' => 'fieldset'
		, '#description' => t('Search your Call Detail Records By Date Range and/or Caller Info')
		, '#collapsible' => FALSE
	);

	$form['cdr_lookup']['cdr_start_time'] = array(
		'#type' => 'textfield'
		, '#title' => t('Start Time')
		, '#size' => 55
		, '#maxlength' => 19
		, '#default_value' => date('Y-m-d H:i:s', mktime() - 2592000) //30 days ago from now
		, '#description' => t('What date would you like to start the search from?  Leave blank to search all dates.'),
	);

	$form['cdr_lookup']['cdr_end_time'] = array(
		'#type' => 'textfield'
		, '#title' => t('End Time')
		, '#size' => 55
		, '#maxlength' => 19
		, '#default_value' => date('Y-m-d H:i:s')
		, '#description' => t('What date would you like to search up to?  Leave blank to search to right now.'),
	);

	$form['cdr_lookup']['cdr_duration_choice'] = array(
		'#type' => 'select'
		, '#title' => t('Call Duration')
		, '#options' => $call_duration_options
		, '#description' => t('Call Duration Criteria'),
	);

	$form['cdr_lookup']['cdr_duration'] = array(
		'#type' => 'textfield'
		, '#title' => t('CdPN')
		, '#size' => 55
		, '#maxlength' => 20
		, '#default_value' => 0
		, '#description' => t('Call Duration (in seconds).'),
	);

	$form['cdr_lookup']['cdr_src_number'] = array(
		'#type' => 'textfield'
		, '#title' => t('CgPN')
		, '#size' => 55
		, '#maxlength' => 30
		, '#description' => t('All, or part of, Calling Party Number (read: the caller).'),
	);

	$form['cdr_lookup']['cdr_dst_number'] = array(
		'#type' => 'textfield'
		, '#title' => t('CdPN')
		, '#size' => 55
		, '#maxlength' => 20
		, '#description' => t('All, or part of, Called Party Number (read: the recipient).'),
	);

	$form['cdr_lookup']['submit'] = array(
		'#type' => 'submit'
		, '#value' => t('Search')
	);
	return $form;
}


/**
 * asteriskcdrs_date_range_validate()
 *
 * Confirm that dates are properly formatted, start time is before end time
 * and that call duration is blank or numeric.
 */
function asteriskcdrs_date_range_validate($form_id, $form_values) {
	//We're only going to offer this to 2029... Sometime in 2038, PHP Date 
	//function loses support anyway ;)
	if(!preg_match('/^[2][0][0-2][0-9]-(0\d|1[0-2])-([0-2]\d|3[0-1]) ((?:(?:[01][\d])|(?:[2][0-3])):[0-5][\d]:[0-5][\d])$/', $form_values['cdr_start_time']) && $form_values['cdr_start_time'] != '')
	{
		form_set_error('cdr_start_time', 'Improperly formed date!  Please use format \'YYYY-MM-DD HH:MM:SS\'');
	}
	if(!preg_match('/^[2][0][0-2][0-9]-(0\d|1[0-2])-([0-2]\d|3[0-1]) ((?:(?:[01][\d])|(?:[2][0-3])):[0-5][\d]:[0-5][\d])$/', $form_values['cdr_end_time']) && $form_values['cdr_end_time'] != '')
	{
		form_set_error('cdr_end_time', 'Improperly formed date!  Please use format \'YYYY-MM-DD HH:MM:SS\'');
	}
	if(strtotime($form_values['cdr_start_time']) > strtotime($form_values['cdr_end_time']))
	{
		form_set_error('', 'Start Time Must Be Before End Time!');
	}
	if(!is_numeric($form_values['cdr_duration']) && $form_values['cdr_duration'] != '')
	{
		form_set_error('cdr_duration', 'Call Duration Must Be Numeric Or Left Blank!');
	}
}


/**
 * asteriskcdrs_date_range_submit()
 *
 * Send user off to results page.
 */
function asteriskcdrs_date_range_submit($form_id, $form_values) {
	//Reformat unset variables, if any, to be zeroes.
	if($form_values['cdr_start_time'] == '')
	{
		$form_values['cdr_start_time'] = 0;
	}
	if($form_values['cdr_end_time'] == '')
	{
		$form_values['cdr_end_time'] = 0;
	}
	if($form_values['cdr_src_number'] == '')
	{
		$form_values['cdr_src_number'] = 0;
	}
	if($form_values['cdr_dst_number'] == '')
	{
		$form_values['cdr_dst_number'] = 0;
	}
	if($form_values['cdr_duration'] == '')
	{
		$form_values['cdr_duration'] = 0;
	}
	//Redirect user to results page with all supplied criteria.
	drupal_goto('asteriskcdrs/results/' . strtotime($form_values['cdr_start_time']) . '/' . strtotime($form_values['cdr_end_time']) . '/' . $form_values['cdr_duration_choice'] . '/' . $form_values['cdr_duration'] . '/' . $form_values['cdr_src_number'] . '/' . $form_values['cdr_dst_number']);
}
/**
 * asteriskcdrs_main_page()
 *
 * Displays a table of Asterisk Call Detail Record (CDR) results to the user
 * for a specified period of time.
 */
function asteriskcdrs_main_page($start_time = 0, $end_time = 0, $duration_check = 'ac', $call_duration = 0, $src_num = 0, $dst_num = 0) {
	//If user's don't supply start/end times, we still want to return all calls,
	//so we need to do lots'o checks here.
	if($start_time != 0)
	{
		$start_time = date('Y-m-d H:i:s', $start_time);
	}
	if($end_time != 0)
	{
		$end_time = date('Y-m-d H:i:s', $end_time);
	}
	if($start_time == 0)
	{
		$where_sql = '';
	}
	else
	{
		if($end_time == 0)
		{
			$where_sql = 'calldate > "' . $start_time . '"';
		}
		else
		{
			$where_sql = 'calldate > "' . $start_time . '"';
			$where_sql .= ' AND calldate < "' . $end_time . '"';
		}
	}

	//Check if specific source number was supplied.
	if($src_num != 0) {
		if($where_sql != '') {
			$where_sql .= ' AND src LIKE \'%' . $src_num . '%\'';
		}
		else {
			$where_sql .= 'src LIKE \'%' . $src_num . '%\'';
		}
	}

	//Check if specific destination number was supplied
	if($dst_num != 0) {
		if($where_sql != '') {
			$where_sql .= ' AND dst LIKE \'%' . $dst_num . '%\'';
		}
		else {
			$where_sql .= 'dst LIKE \'%' . $dst_num . '%\'';
		}
	}

	//Check if duration was specified.
	switch($duration_check) {
		case 'ac':
			break;
		case 'gt':
			if($where_sql != '') {
				$where_sql .= ' AND duration >= ' . $call_duration;
			}
			else {
				$where_sql .= 'duration >= ' . $call_duration;
			}
			break;
		case 'lt':
			if($where_sql != '') {
				$where_sql .= ' AND duration <= ' . $call_duration;
			}
			else {
				$where_sql .= 'duration <= ' . $call_duration;
			}
			break;
		case 'eq':
			if($where_sql != '') {
				$where_sql .= ' AND duration = ' . $call_duration;
			}
			else {
				$where_sql .= 'duration = ' . $call_duration;
			}
			break;
		default:
			break;
	}

	//Instantiate cdr_extra object
	$cdr = new obj_cdr_extra();
	//Set database from config
	$cdr->database = variable_get('asteriskcdrs_cdr_database', db_result(db_query('SELECT DATABASE()')));
	$output = '';
	$output .= t('<b>cdr Data</b>');
	$output .= '<br /><br />';

	//Build table header
	$header = array();
	foreach (variable_get('asteriskcdrs_cdr_columns', array('calldate', 'clid', 'src', 'dst', 'duration')) as $column)
	{
		if($column != '0')
		{
			if($column == 'cdrid')
			{
				$header[] = array('data' => $column, 'field' => $column, 'sort' => 'DESC');
			}
			else
			{
				$header[] = array('data' => $column, 'field' => $column);
			}
		}
	}
	$header[] = array('data' => t('<b>Options</b>'));
	$sql = tablesort_sql($header);

	//Grab results and build table rows
	$table_data = $cdr->find_many_paginated($sql, $where_sql);
	if(db_num_rows($table_data) >= 1) {
		$rows = array();
		while($row_data = db_fetch_object($table_data)){
			$row = array();
			foreach (variable_get('asteriskcdrs_cdr_columns', array('calldate', 'clid', 'src', 'dst', 'duration')) as $column)
			{
				if($column != '0')
				{
					$row[] = array('data' => $row_data->$column);
				}
			}
			$row[] = array('data' => l(t('View Details'), 'asteriskcdrs/results/call_details/' . $row_data->cdrid));
			$rows[] = $row;
		}
	}

	//Theme and paginate table.
	$output .= theme('table', $header, $rows);
	$output .= theme('pager', NULL, variable_get('cdr_paginate', 20));
	$output .= '<br />';
	return $output;
}


/**
 * asteriskcdrs_details_page()
 *
 * Displays a detailed view of a call.
 */
function asteriskcdrs_details_page($call_id = 0) {
	//Instantiate cdr_extra object and load specific call.
	$cdr = new obj_cdr_extra();
	$cdr->database = variable_get('asteriskcdrs_cdr_database', db_result(db_query('SELECT DATABASE()')));
	$cdr->cdrid = $call_id;
	$cdr->find();

	//Define header
	$header = array(
		array('data' => 'Call Detail Records for Call #' . $cdr->cdrid, 'colspan' => 2,),
	);

	//Add table row data from each CDR table column for this call.
	$rows[] = array(
		array('data' => 'Call Detail Record ID: ')
		, array('data' => $cdr->cdrid)
	);

	$rows[] = array(
		array('data' => 'Call Date: ')
		, array('data' => $cdr->calldate)
	);

	$rows[] = array(
		array('data' => 'Caller ID: ')
		, array('data' => $cdr->clid)
	);

	$rows[] = array(
		array('data' => 'Call Source: ')
		, array('data' => $cdr->src)
	);

	$rows[] = array(
		array('data' => 'Call Destination: ')
		, array('data' => $cdr->dst)
	);

	$rows[] = array(
		array('data' => 'Destination Context: ')
		, array('data' => $cdr->dcontext)
	);

	$rows[] = array(
		array('data' => 'Call Channel Name: ')
		, array('data' => $cdr->channel)
	);

	$rows[] = array(
		array('data' => 'Call Destination Channel: ')
		, array('data' => $cdr->dstchannel)
	);

	$rows[] = array(
		array('data' => 'Last Application Executed: ')
		, array('data' => $cdr->lastapp)
	);

	$rows[] = array(
		array('data' => 'Last Application\'s Arguments: ')
		, array('data' => $cdr->lastdata)
	);

	$rows[] = array(
		array('data' => 'Duration of the Call: ')
		, array('data' => $cdr->duration)
	);

	$rows[] = array(
		array('data' => 'Billable Seconds (Duration of the Call, once answered): ')
		, array('data' => $cdr->billsec)
	);

	$rows[] = array(
		array('data' => 'Call Disposition: ')
		, array('data' => $cdr->disposition)
	);

	$rows[] = array(
		array('data' => 'Type of Call Detail Record: ')
		, array('data' => $cdr->amaflags)
	);

	$rows[] = array(
		array('data' => 'Channel\'s Account Code: ')
		, array('data' => $cdr->accountcode)
	);

	$rows[] = array(
		array('data' => 'Channel\'s Unique ID: ')
		, array('data' => $cdr->uniqueid)
	);

	$rows[] = array(
		array('data' => 'Channel\'s User Specified Field: ')
		, array('data' => $cdr->userfield)
	);

	//Theme table and return
	$output = theme('table', $header, $rows);
	return $output;
}