<?php


/**********************************************************************
 * Name: asteriskcdrs_admin.inc
 * Author: Code Generator
 * Date: 2008-01-21
 * 
 * Description: Administrative functions for asteriskcdrs
 *              Drupal Module.
 *********************************************************************/


/**
 * asteriskcdrs_admin_main()
 *
 * This is the main page for all CRUD (Create, Retrieve, Update and Delete)
 * tasks associated with DB tables/classes for this module.
 */
function asteriskcdrs_admin_main() {
	$cdr = new obj_cdr_extra();
	$cdr->database = variable_get('asteriskcdrs_cdr_database', db_result(db_query('SELECT DATABASE()')));
	$output = '';
	$header = array(
		array('data' => t('<b>Table Name</b>')),
		array('data' => t('<b>No Entries</b>')),
	);
	$rows[] = array(
		array('data' => l(t('cdr'), 'admin/asteriskcdrs/cdr')),
		array('data' => $cdr->get_num_rows()),
	);
	$output .= theme('table', $header, $rows);

	if(variable_get('asteriskcdrs_db_fix', 0) == 0 && variable_get('asteriskcdrs_db_setup', 0) == 0) {
		$output .= '<h2>Tasks:</h2>';
	}
	if(variable_get('asteriskcdrs_db_fix', 0) == 0) {
		$output .= l(t('Fix Asterisk Default Database'), 'admin/asteriskcdrs/fixdb') . '<br />';
	}
	if(variable_get('asteriskcdrs_db_setup', 0) == 0) {
		$output .= l(t('Set up local Asterisk CDR Database'), 'admin/asteriskcdrs/setupdb') . '<br />';
	}
	return $output;
}

/**
 * asteriskcdrs_admin_add_cdr()
 *
 * Form hook
 *
 * This function will present the user with a form to add a new entry
 * to the cdr table in the database. 
 */
function asteriskcdrs_admin_add_cdr() {
	$cdr = new obj_cdr_extra();
	$cdr->database = variable_get('asteriskcdrs_cdr_database', db_result(db_query('SELECT DATABASE()')));
	$cdr->cdrid = 0;

	$form['cdrid'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->cdrid,
		'#title' => t('clid'),
		'#disabled' => TRUE,
	);

	$form['calldate'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('calldate'),
		'#required' => TRUE,
	);
	$form['clid'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => '0',
		'#title' => t('clid'),
		'#required' => TRUE,
	);

	$form['src'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('src'),
		'#required' => TRUE,
	);
	$form['dst'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('dst'),
		'#required' => TRUE,
	);
	$form['dcontext'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('dcontext'),
		'#required' => TRUE,
	);
	$form['channel'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('channel'),
		'#required' => TRUE,
	);
	$form['dstchannel'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('dstchannel'),
		'#required' => TRUE,
	);
	$form['lastapp'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('lastapp'),
		'#required' => TRUE,
	);
	$form['lastdata'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('lastdata'),
		'#required' => TRUE,
	);
	$form['duration'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('duration'),
		'#required' => TRUE,
	);
	$form['billsec'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('billsec'),
		'#required' => TRUE,
	);
	$form['disposition'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('disposition'),
		'#required' => TRUE,
	);
	$form['amaflags'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('amaflags'),
		'#required' => TRUE,
	);
	$form['accountcode'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('accountcode'),
		'#required' => TRUE,
	);
	$form['uniqueid'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('uniqueid'),
		'#required' => TRUE,
	);
	$form['userfield'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('userfield'),
		'#required' => TRUE,
	);
	$form['cdr_submit'] = array(
		'#type' => 'submit',
		'#value' => t('Submit'),
	);
	return $form;
}

/**
 * asteriskcdrs_admin_add_cdr_submit()
 *
 * Form _submit hook
 *
 * This function will go through all fields in cdr, setting
 * each to the user-supplied values.  The primary key is set to 0
 * so that the class is aware that this is a new entry.
 */
function asteriskcdrs_admin_add_cdr_submit($form_id, $form_values) {
	$cdr = new obj_cdr_extra();
	$cdr->database = variable_get('asteriskcdrs_cdr_database', db_result(db_query('SELECT DATABASE()')));
	$cdr->cdrid = 0;
	$cdr->find();
	$cdr->calldate = $form_values['calldate'];
	$cdr->src = $form_values['src'];
	$cdr->dst = $form_values['dst'];
	$cdr->dcontext = $form_values['dcontext'];
	$cdr->channel = $form_values['channel'];
	$cdr->dstchannel = $form_values['dstchannel'];
	$cdr->lastapp = $form_values['lastapp'];
	$cdr->lastdata = $form_values['lastdata'];
	$cdr->duration = $form_values['duration'];
	$cdr->billsec = $form_values['billsec'];
	$cdr->disposition = $form_values['disposition'];
	$cdr->amaflags = $form_values['amaflags'];
	$cdr->accountcode = $form_values['accountcode'];
	$cdr->uniqueid = $form_values['uniqueid'];
	$cdr->userfield = $form_values['userfield'];
	$cdr->update();
	drupal_goto('admin/asteriskcdrs/cdr');
}

/**
 * asteriskcdrs_admin_delete_cdr()
 *
 * Form hook
 *
 * This function will find the id from the cdr class and display
 * an uneditable form with existing data, allowing the user to confirm 
 * that they wish to delete this entry.
 */
function asteriskcdrs_admin_delete_cdr($id = 0) {
	$cdr = new obj_cdr_extra();
	$cdr->database = variable_get('asteriskcdrs_cdr_database', db_result(db_query('SELECT DATABASE()')));
	$cdr->cdrid = $id;
	$cdr->find();

	$form['cdrid'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->cdrid,
		'#title' => t('clid'),
		'#disabled' => TRUE,
	);

	$form['calldate'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->calldate,
		'#title' => t('calldate'),
		'#disabled' => TRUE,
	);

	$form['clid'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->clid,
		'#title' => t('clid'),
		'#disabled' => TRUE,
	);

	$form['src'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->src,
		'#title' => t('src'),
		'#disabled' => TRUE,
	);

	$form['dst'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->dst,
		'#title' => t('dst'),
		'#disabled' => TRUE,
	);

	$form['dcontext'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->dcontext,
		'#title' => t('dcontext'),
		'#disabled' => TRUE,
	);

	$form['channel'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->channel,
		'#title' => t('channel'),
		'#disabled' => TRUE,
	);

	$form['dstchannel'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->dstchannel,
		'#title' => t('dstchannel'),
		'#disabled' => TRUE,
	);

	$form['lastapp'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->lastapp,
		'#title' => t('lastapp'),
		'#disabled' => TRUE,
	);

	$form['lastdata'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->lastdata,
		'#title' => t('lastdata'),
		'#disabled' => TRUE,
	);

	$form['duration'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->duration,
		'#title' => t('duration'),
		'#disabled' => TRUE,
	);

	$form['billsec'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->billsec,
		'#title' => t('billsec'),
		'#disabled' => TRUE,
	);

	$form['disposition'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->disposition,
		'#title' => t('disposition'),
		'#disabled' => TRUE,
	);

	$form['amaflags'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->amaflags,
		'#title' => t('amaflags'),
		'#disabled' => TRUE,
	);

	$form['accountcode'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->accountcode,
		'#title' => t('accountcode'),
		'#disabled' => TRUE,
	);

	$form['uniqueid'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->uniqueid,
		'#title' => t('uniqueid'),
		'#disabled' => TRUE,
	);

	$form['userfield'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->userfield,
		'#title' => t('userfield'),
		'#disabled' => TRUE,
	);

	$form['cdr_submit'] = array(
		'#type' => 'submit',
		'#value' => t('Confirm Delete'),
	);
	$form['cdr_cancel'] = array(
		'#value' => l(t('Cancel'), 'admin/asteriskcdrs/cdr'),
	);
	return $form;
}

/**
 * asteriskcdrs_admin_delete_cdr_submit()
 *
 * Form _submit hook
 *
 * This function will find the id from the cdr class and delete
 * the corresponding entry.  Then the user will be sent back to the view
 * function.
 */
function asteriskcdrs_admin_delete_cdr_submit($form_id, $form_values) {
	$cdr = new obj_cdr_extra();
	$cdr->database = variable_get('asteriskcdrs_cdr_database', db_result(db_query('SELECT DATABASE()')));
	$cdr->cdrid = $form_values['cdrid'];
	$cdr->find();
	$cdr->delete();
	drupal_goto('admin/asteriskcdrs/cdr');
}

/**
 * asteriskcdrs_admin_edit_cdr()
 *
 * Form hook
 *
 * This function will find the id from the cdr class and display
 * a form with existing data, allowing the user to change all but the 
 * primary key.
 */
function asteriskcdrs_admin_edit_cdr($id = 0) {
	$cdr = new obj_cdr_extra();
	$cdr->database = variable_get('asteriskcdrs_cdr_database', db_result(db_query('SELECT DATABASE()')));
	$cdr->cdrid = $id;
	$cdr->find();

	$form['cdrid'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->cdrid,
		'#title' => t('clid'),
		'#disabled' => TRUE,
	);

	$form['calldate'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->calldate,
		'#title' => t('calldate'),
		'#required' => TRUE,
	);
	$form['clid'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->clid,
		'#title' => t('clid'),
		'#required' => TRUE,
	);

	$form['src'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->src,
		'#title' => t('src'),
		'#required' => TRUE,
	);
	$form['dst'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->dst,
		'#title' => t('dst'),
		'#required' => TRUE,
	);
	$form['dcontext'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->dcontext,
		'#title' => t('dcontext'),
		'#required' => TRUE,
	);
	$form['channel'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->channel,
		'#title' => t('channel'),
		'#required' => TRUE,
	);
	$form['dstchannel'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->dstchannel,
		'#title' => t('dstchannel'),
		'#required' => TRUE,
	);
	$form['lastapp'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->lastapp,
		'#title' => t('lastapp'),
		'#required' => TRUE,
	);
	$form['lastdata'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->lastdata,
		'#title' => t('lastdata'),
		'#required' => TRUE,
	);
	$form['duration'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->duration,
		'#title' => t('duration'),
		'#required' => TRUE,
	);
	$form['billsec'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->billsec,
		'#title' => t('billsec'),
		'#required' => TRUE,
	);
	$form['disposition'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->disposition,
		'#title' => t('disposition'),
		'#required' => TRUE,
	);
	$form['amaflags'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->amaflags,
		'#title' => t('amaflags'),
		'#required' => TRUE,
	);
	$form['accountcode'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->accountcode,
		'#title' => t('accountcode'),
		'#required' => TRUE,
	);
	$form['uniqueid'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->uniqueid,
		'#title' => t('uniqueid'),
		'#required' => TRUE,
	);
	$form['userfield'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $cdr->userfield,
		'#title' => t('userfield'),
		'#required' => TRUE,
	);
	$form['cdr_submit'] = array(
		'#type' => 'submit',
		'#value' => t('Submit'),
	);
	return $form;
}

/**
 * asteriskcdrs_admin_edit_cdr_submit()
 *
 * Form _submit hook
 *
 * This function will find the id from the cdr class and update
 * the corresponding entry with supplied data.  Then the user will be 
 * sent back to the view function.
 */
function asteriskcdrs_admin_edit_cdr_submit($form_id, $form_values) {
	$cdr = new obj_cdr_extra();
	$cdr->database = variable_get('asteriskcdrs_cdr_database', db_result(db_query('SELECT DATABASE()')));
	$cdr->cdrid = $form_values['cdrid'];
	$cdr->find();
	$cdr->calldate = $form_values['calldate'];
	$cdr->clid = $form_values['clid'];
	$cdr->src = $form_values['src'];
	$cdr->dst = $form_values['dst'];
	$cdr->dcontext = $form_values['dcontext'];
	$cdr->channel = $form_values['channel'];
	$cdr->dstchannel = $form_values['dstchannel'];
	$cdr->lastapp = $form_values['lastapp'];
	$cdr->lastdata = $form_values['lastdata'];
	$cdr->duration = $form_values['duration'];
	$cdr->billsec = $form_values['billsec'];
	$cdr->disposition = $form_values['disposition'];
	$cdr->amaflags = $form_values['amaflags'];
	$cdr->accountcode = $form_values['accountcode'];
	$cdr->uniqueid = $form_values['uniqueid'];
	$cdr->userfield = $form_values['userfield'];
	$cdr->update();
	drupal_goto('admin/asteriskcdrs/cdr');
}

/**
 * asteriskcdrs_admin_view_cdr()
 *
 *
 */
function asteriskcdrs_admin_view_cdr() {
	$cdr = new obj_cdr_extra();
	$cdr->database = variable_get('asteriskcdrs_cdr_database', db_result(db_query('SELECT DATABASE()')));
	$output = '';
	$output .= t('<b>cdr Data</b>');
	$output .= '<br /><br />';
	$header = array();
	foreach (variable_get('asteriskcdrs_cdr_columns', array('calldate', 'clid', 'src', 'dst', 'duration')) as $column)
	{
		if($column != '0')
		{
			if($column == 'cdrid')
			{
				$header[] = array('data' => $column, 'field' => $column, 'sort' => 'DESC');
			}
			else
			{
				$header[] = array('data' => $column, 'field' => $column);
			}
		}
	}
	$header[] = array('data' => t('<b>Options</b>'));
	$sql = tablesort_sql($header);
	$table_data = $cdr->find_many_paginated($sql);
	if(db_num_rows($table_data) >= 1) {
		$rows = array();
		while($row_data = db_fetch_object($table_data)){
			$row = array();
			foreach (variable_get('asteriskcdrs_cdr_columns', array('calldate', 'clid', 'src', 'dst', 'duration')) as $column)
			{
				if($column != '0')
				{
					$row[] = array('data' => $row_data->$column);
				}
			}
			$row[] = array('data' => l(t(Edit), 'admin/asteriskcdrs/cdr/edit/' . $row_data->cdrid) . ' | ' . l(t(Delete), 'admin/asteriskcdrs/cdr/delete/' . $row_data->cdrid));
			$rows[] = $row;
		}
	}
	$output .= theme('table', $header, $rows);
	$output .= theme('pager', NULL, variable_get('cdr_paginate', 20));
	$output .= '<br />';
	return $output;
}

/**
 * Asterisk CDRs Administrative Settings Page
 */
function asteriskcdrs_admin_settings() {
	//Get any _extra_ settings
	$items = array_merge(asteriskcdrs_admin_extra_settings());

	$items['cdr_paginate'] = array(
		'#type'          => 'textfield',
		'#title'         => t('cdr Listings Per Page'),
		'#default_value' => variable_get('cdr_paginate', 20),
		'#size'          => 5,
		'#maxlength'     => 5,
		'#description'   => t('How many entries would you like per cdr admin page?'),
	);

	return system_settings_form($items);
}

