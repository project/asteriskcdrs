<?PHP

	// $Id$

	function quote($value, $force = false)
	{
		// Stripslashes
		if (get_magic_quotes_gpc())
		{
			$value = stripslashes($value);
		}

		// Quote if not integer
		if (!is_numeric($value))
		{
			$value = "'" . mysql_escape_string($value) . "'";
		}
		return $value;
	}

	function allow_null($var)
	{
		//This function is used by the sql builder classes and allows you
		//to use nulls for blank values from page posts etc
		if ($var == '' || $var == '\'\'')
		{
			return 'NULL';
		}
		else
		{
			return $var;
		}
	}

	function get_round_column_text($round)
	{
		switch($round)
		{
			case (1):
				$round_column = 'round_one_score';
				break;
			case (2):
				$round_column = 'round_two_score';
				break;
			case (3):
				$round_column = 'round_three_score';
				break;
			case (4):
				$round_column = 'round_four_score';
				break;
			default:
				drupal_set_message('An error has occured in the scoring. An invalid round was passed to fantasy_golf_tournament_admin_player_update.');
				break;
		};

		return $round_column;
	}


	function fix_xml_data($str_data)
	{
		//The purpose of this function is to "fix" xml data
		//In some cases I have to deal with xml that has its <> translated
		//into &lt;&gt;
		//This function will fix those and return a good xml string
		$str_data = str_replace('&lt;', '<', $str_data);
		$str_data = str_replace('&gt;', '>', $str_data);
		return $str_data;
	}


?>
