<?PHP


include_once('data_helper.php');

/**********************************************************************
 * Class: objcdr
 * Author: Code Generator
 * Date: 2008-01-21
 *********************************************************************/

Class obj_cdr {

	var $database = '';
	var $table_name = 'cdr';

	var $cdrid;
	var $calldate;
	var $clid;
	var $src;
	var $dst;
	var $dcontext;
	var $channel;
	var $dstchannel;
	var $lastapp;
	var $lastdata;
	var $duration;
	var $billsec;
	var $disposition;
	var $amaflags;
	var $accountcode;
	var $uniqueid;
	var $userfield;

	/*****************************************************
	 * Begin constructor
	 ****************************************************/
	// Constructor
	public function __construct() {

		// Initialize class properties
		// useful for when update or insert is called
		// and all values are not set
		$this->cdrid = 0;
		$this->calldate = '';
		$this->clid = 0;
		$this->src = '';
		$this->dst = '';
		$this->dcontext = '';
		$this->channel = '';
		$this->dstchannel = '';
		$this->lastapp = '';
		$this->lastdata = '';
		$this->duration = 0;
		$this->billsec = 0;
		$this->disposition = '';
		$this->amaflags = 0;
		$this->accountcode = '';
		$this->uniqueid = '';
		$this->userfield = '';
	}
	/*****************************************************
	 * End constructor
	 ****************************************************/

	/*****************************************************
	 * Begin fill
	 ****************************************************/
	// Populates a class object with values from another class or an array
	protected function fill_object($obj_data) {

		if( gettype($obj_data) == 'object' ) {

			// Object is another instance of class, assign values accordingly
			$this->cdrid = $obj_data->cdrid;
			$this->calldate = $obj_data->calldate;
			$this->clid = $obj_data->clid;
			$this->src = $obj_data->src;
			$this->dst = $obj_data->dst;
			$this->dcontext = $obj_data->dcontext;
			$this->channel = $obj_data->channel;
			$this->dstchannel = $obj_data->dstchannel;
			$this->lastapp = $obj_data->lastapp;
			$this->lastdata = $obj_data->lastdata;
			$this->duration = $obj_data->duration;
			$this->billsec = $obj_data->billsec;
			$this->disposition = $obj_data->disposition;
			$this->amaflags = $obj_data->amaflags;
			$this->accountcode = $obj_data->accountcode;
			$this->uniqueid = $obj_data->uniqueid;
			$this->userfield = $obj_data->userfield;
		} else {
			$this->cdrid = $obj_data['cdrid'];
			$this->calldate = $obj_data['calldate'];
			$this->clid = $obj_data['clid'];
			$this->src = $obj_data['src'];
			$this->dst = $obj_data['dst'];
			$this->dcontext = $obj_data['dcontext'];
			$this->channel = $obj_data['channel'];
			$this->dstchannel = $obj_data['dstchannel'];
			$this->lastapp = $obj_data['lastapp'];
			$this->lastdata = $obj_data['lastdata'];
			$this->duration = $obj_data['duration'];
			$this->billsec = $obj_data['billsec'];
			$this->disposition = $obj_data['disposition'];
			$this->amaflags = $obj_data['amaflags'];
			$this->accountcode = $obj_data['accountcode'];
			$this->uniqueid = $obj_data['uniqueid'];
			$this->userfield = $obj_data['userfield'];
		}
	}
	/*****************************************************
	 * End fill
	 ****************************************************/

	/*****************************************************
	 * Begin find
	 ****************************************************/
	// Function finds item from cdr based on the primary key
	public function find() {

		$sql = 'SELECT cdrid';
		$sql .= ', calldate';
		$sql .= ', clid';
		$sql .= ', src';
		$sql .= ', dst';
		$sql .= ', dcontext';
		$sql .= ', channel';
		$sql .= ', dstchannel';
		$sql .= ', lastapp';
		$sql .= ', lastdata';
		$sql .= ', duration';
		$sql .= ', billsec';
		$sql .= ', disposition';
		$sql .= ', amaflags';
		$sql .= ', accountcode';
		$sql .= ', uniqueid';
		$sql .= ', userfield';
		$sql .= ' FROM ' . $this->database . '.' . $this->table_name;
		$sql .= ' WHERE cdrid = ' . $this->cdrid;
		$result = db_query($sql);

		if( $row = db_fetch_object($result) ) {
			$this->fill_object($row);
			return true;
		} else {
			return false;
		}
	}
	/*****************************************************
	 * End find
	 ****************************************************/

	/*****************************************************
	 * Begin find_many
	 ****************************************************/
	// Finds all records in table or records mathcing passed where clause
	public function find_many($sql_ext = '', $order_ext = '') {

		$sql = 'SELECT cdrid';
		$sql .= ', calldate';
		$sql .= ', clid';
		$sql .= ', src';
		$sql .= ', dst';
		$sql .= ', dcontext';
		$sql .= ', channel';
		$sql .= ', dstchannel';
		$sql .= ', lastapp';
		$sql .= ', lastdata';
		$sql .= ', duration';
		$sql .= ', billsec';
		$sql .= ', disposition';
		$sql .= ', amaflags';
		$sql .= ', accountcode';
		$sql .= ', uniqueid';
		$sql .= ', userfield';
		$sql .= ' FROM ' . $this->database . '.' . $this->table_name;

		if( $sql_ext != '' ) {
			$sql .= ' WHERE ' . $sql_ext;
		}
		if( $order_ext != '' ) {
			$sql .= ' ' . $order_ext;
		}
		$result = db_query($sql);
		$count = 0;

		while( $row = db_fetch_object($result) ) {
			$item = new obj_cdr();
			$item->fill_object($row);
			$return_data[$count++] = $item;
		}
		return $return_data;
	}
	/*****************************************************
	 * End find_many
	 ****************************************************/

	/*****************************************************
	 * Begin find_many_paginated
	 ****************************************************/
	// Finds all records in table or records mathcing passed where clause
	public function find_many_paginated($sorted_header, $sql_ext = '', $order_ext = '') {
		$sql = 'SELECT cdrid';
		$sql .= ', calldate';
		$sql .= ', clid';
		$sql .= ', src';
		$sql .= ', dst';
		$sql .= ', dcontext';
		$sql .= ', channel';
		$sql .= ', dstchannel';
		$sql .= ', lastapp';
		$sql .= ', lastdata';
		$sql .= ', duration';
		$sql .= ', billsec';
		$sql .= ', disposition';
		$sql .= ', amaflags';
		$sql .= ', accountcode';
		$sql .= ', uniqueid';
		$sql .= ', userfield';
		$sql .= ' FROM ' . $this->database . '.' . $this->table_name;

		if( $sql_ext != '' ) {
			$sql .= ' WHERE ' . $sql_ext;
		}
		if( $order_ext != '' ) {
			$sql .= ' ' . $order_ext;
		}
		$result_count = '
			SELECT
				count(cdrid) as this_count
			FROM
			(
				' . $sql . '
			) count_table
		';
		$result = pager_query
		(
			db_rewrite_sql
			('
				' . $sql . '
				' . $sorted_header
				, 'cdr'
				, 'cdrid'
			),
			variable_get('cdr_paginate', 20),
			0,
			$result_count
		);
		return $result;
	}
	/*****************************************************
	 * End find_many_paginated
	 ****************************************************/

	/*****************************************************
	 * Begin get_num_rows
	 ****************************************************/
	// This function will return the number of rows/entries in a table
	public function get_num_rows() {

		$sql = 'SELECT COUNT(cdrid) as total_entries FROM ' . $this->database . '.' . $this->table_name;
		$num_rows = db_fetch_object(db_query($sql));
		return $num_rows->total_entries;
	}
	/*****************************************************
	 * End get_num_rows
	 ****************************************************/

	/*****************************************************
	 * Begin update
	 ****************************************************/
	// Updates database with object properties if primary key is set or creates new entry if no primary key
	public function update() {

		if( $this->clid == 0 ) {
			$sql = 'INSERT INTO ' . $this->database . '.' . $this->table_name . ' ( ';
			$sql .= 'calldate';
			$sql .= ', clid';
			$sql .= ', src';
			$sql .= ', dst';
			$sql .= ', dcontext';
			$sql .= ', channel';
			$sql .= ', dstchannel';
			$sql .= ', lastapp';
			$sql .= ', lastdata';
			$sql .= ', duration';
			$sql .= ', billsec';
			$sql .= ', disposition';
			$sql .= ', amaflags';
			$sql .= ', accountcode';
			$sql .= ', uniqueid';
			$sql .= ', userfield';
			$sql .= ') VALUES (';
			$sql .= '' . quote($this->calldate);
			$sql .= ', ' . quote($this->clid);
			$sql .= ', ' . quote($this->src);
			$sql .= ', ' . quote($this->dst);
			$sql .= ', ' . quote($this->dcontext);
			$sql .= ', ' . quote($this->channel);
			$sql .= ', ' . quote($this->dstchannel);
			$sql .= ', ' . quote($this->lastapp);
			$sql .= ', ' . quote($this->lastdata);
			$sql .= ', ' . $this->duration;
			$sql .= ', ' . $this->billsec;
			$sql .= ', ' . quote($this->disposition);
			$sql .= ', ' . $this->amaflags;
			$sql .= ', ' . quote($this->accountcode);
			$sql .= ', ' . quote($this->uniqueid);
			$sql .= ', ' . quote($this->userfield);
			$sql .= ')';
		} else {
			$sql = 'UPDATE ' . $this->database . '.' . $this->table_name;
			$sql .= ' SET ';
			$sql .= 'calldate = ' . quote($this->calldate, true);
			$sql .= ', clid = ' . quote($this->clid, true);
			$sql .= ', src = ' . quote($this->src, true);
			$sql .= ', dst = ' . quote($this->dst, true);
			$sql .= ', dcontext = ' . quote($this->dcontext, true);
			$sql .= ', channel = ' . quote($this->channel, true);
			$sql .= ', dstchannel = ' . quote($this->dstchannel, true);
			$sql .= ', lastapp = ' . quote($this->lastapp, true);
			$sql .= ', lastdata = ' . quote($this->lastdata, true);
			$sql .= ', duration = ' . $this->duration;
			$sql .= ', billsec = ' . $this->billsec;
			$sql .= ', disposition = ' . quote($this->disposition, true);
			$sql .= ', amaflags = ' . $this->amaflags;
			$sql .= ', accountcode = ' . quote($this->accountcode, true);
			$sql .= ', uniqueid = ' . quote($this->uniqueid, true);
			$sql .= ', userfield = ' . quote($this->userfield, true);
			$sql .= ' WHERE cdrid = ' . $this->cdrid;
		}
		db_query($sql);

		if( $this->clid == 0 ) {
			$result = db_query('SELECT @@IDENTITY ident FROM ' . $this->database . '.' . $this->table_name);
			$data = db_fetch_object($result);
			$this->clid = $data->ident;
			$this->find();
		}

	}
	/*****************************************************
	 * End update
	 ****************************************************/

	/*****************************************************
	 * Begin delete
	 ****************************************************/
	// This function will delete the record from table specified by primary key
	public function delete() {

		$sql = 'DELETE FROM ' . $this->database . '.' . $this->table_name . ' WHERE cdrid = ' . $this->cdrid;
		db_query($sql);
	}
	/*****************************************************
	 * End delete
	 ****************************************************/

	/*****************************************************
	 * Begin destructor
	 ****************************************************/
	public function __destruct() {
		unset($this->cdrid, $this->calldate, $this->clid, $this->src, $this->dst, $this->dcontext, $this->channel, $this->dstchannel, $this->lastapp, $this->lastdata, $this->duration, $this->billsec, $this->disposition, $this->amaflags, $this->accountcode, $this->uniqueid, $this->userfield);
	}
	/*****************************************************
	 * End destructor
	 ****************************************************/

}
